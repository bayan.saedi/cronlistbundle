<?php


use Bayan\CronListBundle\BayanCronListBundle;
use Bayan\CronListBundle\Services\CronList;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;

class FunctionalTest extends TestCase
{
    public function testService()
    {
        $kernel = new CronListTestingKernel([
            'namespace' => "App\\Command",
            'path' => "src/Command"
        ]);
        $kernel->boot();
        $container = $kernel->getContainer();
        $service = $container->get('cron_list_bundle.cron_list');
        $this->assertInstanceOf(CronList::class, $service);

    }
}

class CronListTestingKernel extends Kernel
{
    /**
     * @var array
     */
    private $cronListConfig;

    public function __construct(array $cronListConfig = [])
    {
        parent::__construct('test', true);
        $this->cronListConfig = $cronListConfig;
    }

    /**
     * @inheritDoc
     */
    public function registerBundles()
    {
        return [
            new BayanCronListBundle()
        ];
    }

    /**
     * @inheritDoc
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(function (ContainerBuilder $container) {
            $container->loadFromExtension('bayan_cron_list', $this->cronListConfig);
        });
    }
}
