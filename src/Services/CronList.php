<?php


namespace Bayan\CronListBundle\Services;


use Bayan\CronListBundle\Annotation\CronWatcher;
use Cron\CronExpression;
use Doctrine\Common\Annotations\AnnotationReader;
use ReflectionClass;
use SplFileInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class CronList
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $namespace;
    private $path;
    private $annotationReader;

    public function __construct(ContainerInterface $container, $namespace, $path)
    {
        $this->container = $container;
        $this->namespace = $namespace;
        $this->path = $path;
    }

    public function getCornList(): array
    {
        $dir = $this->container->getParameter('kernel.project_dir');
        $dir = $dir . '/' . $this->path;
        $filesystem = new Filesystem();
        $finder = new Finder();
        $commands = [];

        if ($filesystem->exists($dir)) {
            $finder->files()->in($dir);
            if ($finder->hasResults()) {
                foreach ($finder as $key => $file) {

                    $annotation = $this->readAnnotation($file);

                    if (!$annotation) {
                        continue;
                    }

                    $commands[$key]['name'] = $annotation->name;
                    $commands[$key]['schedule'] = $annotation->schedule;
                }
            }
        }
        return $commands;
    }

    private function readAnnotation(SplFileInfo $file)
    {
        $class = $this->namespace . '\\' . $file->getBasename('.php');
        $reader = new AnnotationReader();
        $annotation = $reader->getClassAnnotation(
            new ReflectionClass($class),
            CronWatcher::class
        );
        if (!$annotation) {
            return;
        }

        $this->checkingScheduleValidation($annotation);

        return $annotation;
    }

    private function checkingScheduleValidation($annotation)
    {
        $pattern = "\/";
        $replacement = '/';
        $annotation->schedule = str_replace($pattern, $replacement, $annotation->schedule);

        return CronExpression::factory($annotation->schedule);
    }

}
