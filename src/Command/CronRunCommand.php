<?php

namespace Bayan\CronListBundle\Command;

use Bayan\CronListBundle\Services\CronList;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;
use Bayan\CronListBundle\Entity\CronErrorReport;


class CronRunCommand extends Command
{
    protected static $defaultName = 'cron:run';
    /**
     * @var CronList
     */
    private $cronListService;
    /**
     * @var ContainerInterface
     */
    private $container;

    private $em;

    public function __construct(ContainerInterface $container, CronList $cronListService)
    {
        parent::__construct();
        $this->cronListService = $cronListService;
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /**
         * Another way to get all commands in a project and read annotations
         */
//        $commands = $this->getApplication()->all();
//        $commands = [];
//        foreach ($commands as $command) {
//            $reflectedClass = new \ReflectionClass($command);
//
//            $annotation = $this->annotationReader->getClassAnnotation($reflectedClass, CronWatcher::class);
//            if (!$annotation) {
//                continue;
//            }
//             $commands[] = $annotation;
//
//        }

        $phpBinaryFinder = new PhpExecutableFinder();
        $phpBinaryPath = $phpBinaryFinder->find();
        $pathToProject = $this->container->getParameter('kernel.project_dir') . '/bin/console';

        $commands = $this->cronListService->getCornList();

        foreach ($commands as $command) {


            $process = new Process([$phpBinaryPath, $pathToProject, $command['name']]);
            $process->run();

            if (!$process->isSuccessful()) {
                $error = new ProcessFailedException($process);
                $this->saveErrorMessage($command, $error->getMessage());
                $io = new SymfonyStyle($input, $output);
                $io->error($process->getOutput() . $error->getMessage());;
            } else {

                $io = new SymfonyStyle($input, $output);
                $io->success($process->getOutput());
            }

        }

//        $arg1 = $input->getArgument('arg1');
//
//        if ($arg1) {
//            $io->note(sprintf('You passed an argument: %s', $arg1));
//        }
//
//        if ($input->getOption('option1')) {
//            // ...
//        }


        return 0;
    }

    public function saveErrorMessage($command, string $errorMessage)
    {
        /**
         * @var CronErrorReport $cronReport
         */
        $cronReport = new CronErrorReport();
        $cronReport->setCommand($command['name']);
        $cronReport->setErrorMessage($errorMessage);
        $this->em->persist($cronReport);
        $this->em->flush();
    }
}
