<?php


namespace Bayan\CronListBundle\Annotation;


/**
 * @Annotation
 * @Target({"CLASS"})
 */
class CronWatcher
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $schedule;

    public function getName() {
        return $this->name;
    }

    public function getSchedule() {
        return $this->schedule;
    }
}
