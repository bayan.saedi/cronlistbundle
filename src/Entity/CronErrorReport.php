<?php

namespace Bayan\CronListBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CronErrorReport
 *
 * @ORM\Table(name="cron_error_report")
 * @ORM\Entity(repositoryClass="Cron\CronBundle\Entity\CronErrorReportRepository")
 */
class CronErrorReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="command", type="string", length=1024)
     */
    private $command;

    /**
     * @var string
     *
     * @ORM\Column(name="error_message", type="string", length=1024)
     */
    private $errorMessage;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $command
     * @return CronErrorReport
     */
    public function setCommand($command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Set errorMessage
     *
     * @param  string  $errorMessage
     * @return CronErrorReport
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    /**
     * Get errorMessage
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
}
