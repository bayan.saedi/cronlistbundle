<?php


namespace Bayan\CronListBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('bayan_cron_list');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->variableNode('namespace')->isRequired()->end()
            ->variableNode('path')->isRequired()->end()
            ->end();
        return $treeBuilder;
    }
}
